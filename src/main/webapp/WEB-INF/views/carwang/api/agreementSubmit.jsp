<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="ko" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="ko"> 
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover"/>
	<meta name="format-detection" content="telephone=no, address=no, email=no"/> <!-- 숫자 입력시 전화번호, 지도, 이메일 사용금지 -->
	<meta name="robots" content="noindex, nofollow" />

	<title></title>
	
	<script type="text/javascript" src="${JS }/jquery-1.11.3.min.js"></script>
	
	<!-- password hash -->
	<script type="text/javascript">
	$(function(){
		
		$('#frm').attr('action', '${agreementUrl}').submit();
		
	});

	</script>
</head>
<body>
	<form id="frm" name="frm" method="POST" >
		<input type="hidden" id="token" name="token" value="Bearer ${user.accessToken }">
		<input type="hidden" id="state" name="state" value="ahope">
	</form>
</body>
</html>