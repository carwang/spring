package kr.co.ahope.common.ascpect;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Aspect
@Component("LogAspect")
public class LogAspect {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	@Before("execution(* kr.co.ahope..controller.*Controller.*(..))")
	public void beforeControllerLog(JoinPoint jp){
		try {
			String className = jp.getTarget().getClass().getSimpleName();
			String methodName = jp.getSignature().getName();
			
			logger.info("[ST][" + className + " - " + methodName + "]");
			
			Object[] objescts = jp.getArgs();
			for(Object obj : objescts) {
				if (obj instanceof HttpServletRequest) {
					HttpServletRequest request = (HttpServletRequest)obj;
					Enumeration<String> enumer = request.getParameterNames();
					if (enumer.hasMoreElements()) {
						logger.debug("==["+methodName+" request parameter]==");
						while(enumer.hasMoreElements()) {
							String key = enumer.nextElement();
							if (!key.equals("pw")){
								String reqParam = request.getParameter(key)!=null?request.getParameter(key):"";
								if (key.equals("ctn") || key.equals("managerCtn")) {
									reqParam = maskingCtn(reqParam);
								} else if (key.indexOf("deviceCtn") > -1 || key.indexOf("ctnTemp") > -1) {
									String deviceIndexs = request.getParameter("deviceIndexs")!=null?request.getParameter("deviceIndexs"):"";
									String[] deviceIndexArray = deviceIndexs.split(",");
									for (int i=0; i<deviceIndexArray.length; i++) {
										if (key.equals("deviceCtn"+deviceIndexArray[i]) || key.equals("ctnTemp"+deviceIndexArray[i])) {
											reqParam = maskingCtn(reqParam);
											break;
										}
									}
								}
								logger.debug("["+key+"]:"+reqParam);
							}
						}
					}
					break;
				} else if (obj instanceof Map) {
					Map<String, Object> map = (Map<String, Object>)obj;
					if (!map.isEmpty()) {
						Iterator<String> iter = map.keySet().iterator();
						logger.debug("==["+methodName+" request parameter]==");
						while(iter.hasNext()) {
							String key = iter.next();
							if (!key.equals("pw")){							
								if ( "org.springframework.core.convert.ConversionService".equals(key) ) {
									continue;
								}
								String reqParam = map.get(key)!=null?map.get(key).toString():"";
								if (key.equals("ctn") || key.equals("managerCtn")) {
									reqParam = maskingCtn(reqParam);
								} else if (key.indexOf("deviceCtn") > -1 || key.indexOf("ctnTemp") > -1) {
									String deviceIndexs = map.get("deviceIndexs")!=null?map.get("deviceIndexs").toString():"";
									String[] deviceIndexArray = deviceIndexs.split(",");
									for (int i=0; i<deviceIndexArray.length; i++) {
										if (key.equals("deviceCtn"+deviceIndexArray[i]) || key.equals("ctnTemp"+deviceIndexArray[i])) {
											reqParam = maskingCtn(reqParam);
											break;
										}
									}
								}
								logger.debug("["+key+"]:"+reqParam);
							}
						}
					}
					break;
				} else if (obj instanceof String) {
					logger.debug("==["+methodName+" request parameter]==");
					logger.debug((String)obj);
					break;
				} else if (obj instanceof MultipartFile) {
					
				} else {
					if(obj != null)
						logger.debug(obj.toString());
				}
			}
		} catch (Exception e) {
			logger.info("beforeControllerLog error");
			e.printStackTrace();
		}
	}
	
	@After("execution(* kr.co.ahope..controller.*Controller.*(..))")
	public void afterControllerLog(JoinPoint jp){
		String className = jp.getTarget().getClass().getSimpleName();
		String methodName = jp.getSignature().getName();
		logger.info("[ED][" + className + " - " + methodName + "]");
	}
	
	@Before("execution(* kr.co.ahope..service.*Service.*(..))")
	public void beforeServiceLog(JoinPoint jp){
		String className = jp.getTarget().getClass().getSimpleName();
		String methodName = jp.getSignature().getName();
		logger.info("[ST][" + className + " - " + methodName + "]");
	}
	
	@After("execution(* kr.co.ahope..service.*Service.*(..))")
	public void afterServiceLog(JoinPoint jp){
		String className = jp.getTarget().getClass().getSimpleName();
		String methodName = jp.getSignature().getName();
		logger.info("[ED][" + className + " - " + methodName + "]");
	}
	
	public String maskingCtn(String ctn) {
		if (ctn != null) {
			if (ctn.length() > 4) {
				StringBuilder maskingParam = new StringBuilder();
				for (int i = 0; i < ctn.length()-4; i++) {
					maskingParam.append('*');
				}
				return maskingParam+ctn.substring(ctn.length()-4, ctn.length());
			} else {
				return ctn;
			}
		} else {
			return "";
		}
	}
	
}
