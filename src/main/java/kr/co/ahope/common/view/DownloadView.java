package kr.co.ahope.common.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import kr.co.ahope.common.bean.FileInfo;
import kr.co.ahope.common.util.CommonUtil;

@Component("downloadView")
public class DownloadView extends AbstractView {
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		OutputStream out = null;
		FileInputStream fis = null;
		
		FileInfo fileInfo = (FileInfo)model.get("fileInfo");
		
		try {
			File file = new File(fileInfo.getFilePath());
			fis = new FileInputStream(file);
			
			String fileName = fileInfo.getFileName();
			String userAgent = CommonUtil.nvl(request.getHeader("User-Agent"));
			if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
			    fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+","\\ ");
			} else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
				fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "\\ ");
			} else if (userAgent.indexOf("Trident") > -1) { //MS IE 11
				fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "\\ ");
			} else { // 모질라나 오페라
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1").replaceAll("\\+", "\\ ");
			}
			
			response.setContentLength((int) file.length());
			response.setContentType("binary/octet-stream; charset=utf-8");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
			
			out = response.getOutputStream();
			FileCopyUtils.copy(fis, out);
			out.flush();
			
		} catch (Exception e) {
			//TODO
			e.printStackTrace();
		} finally {
			if(out != null) out.close();
			if(fis != null) fis.close();
		}
	}
}