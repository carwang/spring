package kr.co.ahope.common.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.api.user.dao.UserAPIDAO;
import kr.co.ahope.common.util.CommonUtil;

public class ApiAuthInterceptor implements HandlerInterceptor {

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	@Autowired
	private UserAPIDAO dao; 
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
		MDC.put("RID", CommonUtil.getRandomStr());
		// TODO Auto-generated method stub
		

		String apiToken = req.getHeader("apiToken");
		User user = dao.getUserWithAPIToken(apiToken);
		if(user == null || user.getUserSeq() == null) {
			//TODO api token 없음 에러 처리
			writeResponseMsg(res, "401", "Unauthorized");
			return false;
		}
		
		req.setAttribute("user", user);
		return true;
	}
	
	public static String getBody(HttpServletRequest request) throws IOException {
        String buffer;
        BufferedReader input = new BufferedReader(new InputStreamReader((InputStream)request.getInputStream()));
        StringBuilder builder = new StringBuilder();
        while ((buffer = input.readLine()) != null) {
            if (builder.length() > 0) {
                builder.append("\n");
            }
            builder.append(buffer);
        }
        return builder.toString();
    }

	public void writeResponseMsg(HttpServletResponse res, String status, String msg) {
		res.setHeader("Content-Type", "application/json;charset=UTF-8");
		res.setStatus(Integer.parseInt(status));
		String output = "{\"status\":\""+ status + "\", \"msg\":\"" + msg + "\"}";
		OutputStream os = null;
		try {
			os = res.getOutputStream();
			os.write(output.getBytes());
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if(os != null) {
				try {
					os.close();
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
				os = null;
			}
		} 
	}
}
