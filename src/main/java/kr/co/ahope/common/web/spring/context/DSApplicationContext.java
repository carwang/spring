package kr.co.ahope.common.web.spring.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class DSApplicationContext implements ApplicationContextAware {

	private Logger logger = LoggerFactory.getLogger(DSApplicationContext.class);
	private static ApplicationContext CONTEXT;

	public DSApplicationContext() {
		logger.debug("init DSApplicationContext");
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		CONTEXT = context;
	}

	public static Object getBean(String beanName) {
		return CONTEXT.getBean(beanName);
	}

	public static <T> T getBean(String beanName, Class<T> requiredType) {
		return CONTEXT.getBean(beanName, requiredType);
	}

}
