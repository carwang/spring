package kr.co.ahope.common.web.spring.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Source;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import kr.co.ahope.common.interceptor.ApiAuthInterceptor;
import kr.co.ahope.common.web.client.HyundaiRestTemplate;
import kr.co.ahope.common.web.spring.adapter.AhopeRequestMappingHandlerAdapter;
import kr.co.ahope.common.web.spring.resolver.RequestAttributeArgumentResolver;
import kr.co.ahope.common.web.spring.resolver.RequestJsonArgumentResolver;
import kr.co.ahope.common.web.spring.resolver.RequestParamMapArgumentResolver;

@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "kr.co.ahope")
public class ServletContextConfig implements WebMvcConfigurer {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@Bean 
	public ThreadPoolTaskExecutor taskExcutor() {
		ThreadPoolTaskExecutor taskExcutor = new ThreadPoolTaskExecutor();
		taskExcutor.setCorePoolSize(5);
		taskExcutor.setMaxPoolSize(10);
		taskExcutor.setQueueCapacity(25);
		return taskExcutor;
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public HyundaiRestTemplate hyundaiRestTemplate() {
		return new HyundaiRestTemplate();
	}
	
	@Bean
	public ViewResolver beanNameViewResolver() {
		BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
		beanNameViewResolver.setOrder(0);
		return beanNameViewResolver;
	}

	@Bean
	public ViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setOrder(1);
		internalResourceViewResolver.setPrefix("/WEB-INF/views/carwang");
		internalResourceViewResolver.setSuffix(".jsp");

		return internalResourceViewResolver;
	}

	@Bean(name = "jsonView")
	public View mappingJacksonJsonView() {
		return new MappingJackson2JsonView();
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		// multipartResolver.setMaxUploadSize(1024*1024*500);
		return multipartResolver;
	}

	/**
	 * Custom Annotation을 등록
	 * 
	 * @return
	 */
	@Bean
	public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
		RequestMappingHandlerAdapter adapter = new AhopeRequestMappingHandlerAdapter();

		// set CustomAdaptor
		List<HandlerMethodArgumentResolver> argumentResolvers = new ArrayList<HandlerMethodArgumentResolver>();
		argumentResolvers.add(new RequestAttributeArgumentResolver());
		argumentResolvers.add(new RequestParamMapArgumentResolver());
		argumentResolvers.add(new RequestJsonArgumentResolver());
		adapter.setCustomArgumentResolvers(argumentResolvers);

		List<HttpMessageConverter<?>> messageConverters = adapter.getMessageConverters();

		messageConverters.add(new ByteArrayHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new SourceHttpMessageConverter<Source>());
		messageConverters.add(new AllEncompassingFormHttpMessageConverter());
		messageConverters.add(new ResourceHttpMessageConverter());
		messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
		messageConverters.add(new MappingJackson2HttpMessageConverter());
//		messageConverters.add(new MappingJacksonHttpMessageConverter());
		adapter.setMessageConverters(messageConverters);

		return adapter;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		// registry.addResourceHandler("/favicon.ico").addResourceLocations("/resources/favicon.ico");
	}

	@Bean
	public ApiAuthInterceptor apiAuthInterceptor() {
		return new ApiAuthInterceptor();
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		interceptor.setParamName("lang");
		return interceptor;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		InterceptorRegistration apiAuth = registry.addInterceptor(apiAuthInterceptor());
			apiAuth.addPathPatterns("/api/**")
			.excludePathPatterns("/api/user/**")
			.excludePathPatterns("/api/car/agreement");

		registry.addInterceptor(localeChangeInterceptor());
	}
}
