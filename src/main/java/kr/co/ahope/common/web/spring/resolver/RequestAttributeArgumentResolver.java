package kr.co.ahope.common.web.spring.resolver;

import java.lang.annotation.Annotation;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import kr.co.ahope.common.util.CommonUtil;
import kr.co.ahope.common.web.spring.annotation.RequestAttribute;

public class RequestAttributeArgumentResolver implements HandlerMethodArgumentResolver {

	@SuppressWarnings("unchecked")
	public Object resolveArgument(MethodParameter param,
			ModelAndViewContainer mnv, NativeWebRequest request,
			WebDataBinderFactory beanFactory) throws Exception {
		
		Annotation[] paramAnns = param.getParameterAnnotations();  
		Class<?> paramType = param.getParameterType();  
		   
        for (Annotation paramAnn : paramAnns) {  
            if (RequestAttribute.class.isInstance(paramAnn)) {  
            	
            	RequestAttribute reqAttr = (RequestAttribute) paramAnn;  
            	HttpServletRequest httprequest = (HttpServletRequest) request.getNativeRequest();
            	
            	Object result = null;
            	if(Map.class.isAssignableFrom(paramType)){
            		if(reqAttr.value().equals("")) {
            			Map<String, Object> attrMap = (paramType.isAssignableFrom(Map.class)) ? new HashMap<String, Object>() : (Map<String, Object>)paramType.getDeclaredConstructor().newInstance();
            			
            			String name = null;
						Enumeration<String> names = httprequest.getAttributeNames();
            			while(names.hasMoreElements()) {
            				name = names.nextElement();
            				attrMap.put(name, httprequest.getAttribute(name));
            			}
            			result = attrMap;
            		} else {
            			result = httprequest.getAttribute(reqAttr.value());
            		}
            	} else {
            		result = httprequest.getAttribute(reqAttr.value());
            	}
            	
            	if(reqAttr.required() && CommonUtil.isNull(result)) {
            		throw new Exception("Required Parameter is Null");
            	}
            	
                return result;  
            }  
        }  
		
		return WebArgumentResolver.UNRESOLVED;
	}

	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(RequestAttribute.class);
	}
	
}
