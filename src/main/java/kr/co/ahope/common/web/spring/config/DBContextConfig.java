package kr.co.ahope.common.web.spring.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

@Configuration
public class DBContextConfig {

	@Value("${jdbc.driver}")
	private String DRIVER_CLASS_NAME;
	
	@Value("${jdbc.url}")
	private String URL;
	
	@Value("${jdbc.user}")
	private String USER;
	
	@Value("${jdbc.password}")
	private String PASSWORD;
	
	//TODO tomcat connection pool로 설정 변경 필요
	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(DRIVER_CLASS_NAME);
		dataSource.setUrl(URL);
		dataSource.setUsername(USER);
		dataSource.setPassword(PASSWORD);
		
		dataSource.setInitialSize(2);
		dataSource.setMaxActive(10);
		dataSource.setMinIdle(2);
		dataSource.setMaxWait(3000);
		dataSource.setPoolPreparedStatements(true);
		dataSource.setMaxOpenPreparedStatements(10);
		
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setTestWhileIdle(true);
		dataSource.setTimeBetweenEvictionRunsMillis(360000);
		return dataSource;
	}
	
	
	@Bean
	public SqlSessionFactoryBean sqlSessionFactory(ApplicationContext applicationContext) throws Exception {
		
		//Resource resource = applicationContext.getResource("classpath:");
		String classPath = applicationContext.getResource("classpath:").getFile().getPath();
		if (classPath.indexOf("test-classes") > -1) {
			classPath = new File(classPath).getParent() + "/classes";
		}
		
		List<String> beanClassNameList = searchBeanClassName(classPath);
		Class<?>[] aliasClasses = new Class[beanClassNameList.size()];
		for(int idx = 0; idx < beanClassNameList.size(); idx++) {
			String beanClassName  = beanClassNameList.get(idx);
			Class<?> clazz = Class.forName(beanClassName);
			aliasClasses[idx] = clazz;
		}
		
		org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
		config.setMapUnderscoreToCamelCase(true);
		
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setConfiguration(config);
		factoryBean.setTypeAliases(aliasClasses);
		factoryBean.setDataSource(dataSource());
		factoryBean.setMapperLocations(applicationContext.getResources("classpath:kr/co/ahope/**/sql/*.xml"));
		return factoryBean;
	}
	
	@Bean
	public SqlSessionTemplate sqlSessionTemplate(ApplicationContext applicationContext) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory(applicationContext).getObject());
	}

	//bean package안에 있는 class name 추출
	public List<String> searchBeanClassName(String source) {
		List<String> aliasClasseList = new ArrayList<String>(); 
		try {
			File dir = new File(source); 
			File[] fileList = dir.listFiles();
			if(fileList != null) {
				for(int i = 0 ; i < fileList.length ; i++){
					File file = fileList[i]; 
					if(file.isDirectory() && file.getName().equals("bean")){
						File[] beanList = file.listFiles();
						for(File bean : beanList) {
							String path = bean.getPath();
							path = path.substring(path.indexOf("classes") + 8, path.length());
							path = path.replaceAll("\\\\", "/").replaceAll("/", ".").replaceAll(".class", "");
							aliasClasseList.add(path);
						}
					} else {
						aliasClasseList.addAll(searchBeanClassName(file.getCanonicalPath().toString()));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aliasClasseList;
	}
	
	@Bean
	public DataSourceTransactionManager transactionManager(DataSource dataSource){
		return new DataSourceTransactionManager(dataSource);
	}
}
