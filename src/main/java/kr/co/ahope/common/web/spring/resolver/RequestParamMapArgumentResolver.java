package kr.co.ahope.common.web.spring.resolver;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import kr.co.ahope.common.bean.ParamMap;
import kr.co.ahope.common.web.spring.annotation.RequestParamMap;

public class RequestParamMapArgumentResolver implements HandlerMethodArgumentResolver {

	public Object resolveArgument(MethodParameter param,
			ModelAndViewContainer mnv, NativeWebRequest request,
			WebDataBinderFactory binderFactory) throws Exception {
		
		Annotation[] paramAnns = param.getParameterAnnotations();  
		Class<?> paramType = param.getParameterType();  
		   
        for (Annotation paramAnn : paramAnns) {
        	
        	if (RequestParamMap.class.isInstance(paramAnn)) {  

        		if(paramType.isAssignableFrom(ParamMap.class)){
	            	HttpServletRequest httprequest = (HttpServletRequest) request.getNativeRequest();
	            	return new ParamMap(httprequest); 
	            } else {
	            	return WebArgumentResolver.UNRESOLVED;
	            }
        	}
        }  
		
		return WebArgumentResolver.UNRESOLVED;
	}

	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(RequestParamMap.class);
	}
	
}
