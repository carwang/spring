package kr.co.ahope.common.web.spring.config;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import kr.co.ahope.common.web.spring.context.DSApplicationContext;
import kr.co.ahope.common.web.spring.properties.PropertiesLoader;

@Configuration
public class RootContextConfig {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static Resource[] getPropertiesResources(String active) {
		List<Resource> resourceList = new ArrayList<Resource>(); 
		try {
			String propertyPath = RootContextConfig.class.getResource("/").getPath() + "properties/";
			File[] files = new File(propertyPath).listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					File[] propertyFiles = file.listFiles();
					for (File propertyFile : propertyFiles) {
						if (propertyFile.getPath().indexOf("_" + active + ".properties") > -1) {
							resourceList.add(new FileSystemResource(propertyFile));
						}
					}
				} else if (file.getPath().indexOf("_" + active + ".properties") > -1) {
					resourceList.add(new FileSystemResource(file));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Resource[] resources = new Resource[resourceList.size()];
		for (int idx = 0; idx < resourceList.size(); idx++){
			resources[idx] = resourceList.get(idx);
		}
		
		return resources;
	}
	
	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		Properties prop = PropertiesLoader.load("spring.properties");
		String active = prop.getProperty("spring.active", "local");
		PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
		pspc.setLocations(getPropertiesResources(active));
		return pspc;
	}
	
	@Bean
	public ApplicationContextAware dsApplicationContext() {
		return new DSApplicationContext();
	}
}

