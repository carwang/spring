package kr.co.ahope.common.web.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import kr.co.ahope.carwang.api.user.bean.TokenResponse;
import kr.co.ahope.common.bean.HyundaiError;
import kr.co.ahope.common.exception.LogicException;
import kr.co.ahope.common.util.Base64Encoder;

public class HyundaiRestTemplate extends RestTemplate {
	
	private <T> T callHyundaiRest(String url, HttpMethod method, String body, HttpHeaders headers, Class<T> clazz) throws Exception {
		HttpEntity<String> requestEntity =  new HttpEntity<String>(body, headers);
		ResponseEntity<String> jsonRes = exchange(url, method, requestEntity, String.class);
		//TODO
		System.out.println(jsonRes.getBody());
		if (jsonRes.getStatusCodeValue() != 200) {
			logger.error("["+jsonRes.getStatusCodeValue() + "]:"+jsonRes.getBody());
			HyundaiError error = new Gson().fromJson(jsonRes.getBody(), HyundaiError.class);
			throw new LogicException(error.getErrCode(), error.getErrMsg());
		}
		return new Gson().fromJson(jsonRes.getBody(), clazz);
	}
	
	public <T> T postHyundaiToken(String url, String clientId, String secret, String param, Class<T> clazz) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		System.out.println("basic " + new String(Base64Encoder.encode((clientId+":"+secret).getBytes()), "utf-8"));
		headers.add("Authorization", "Basic " + new String(Base64Encoder.encode((clientId+":"+secret).getBytes()), "utf-8"));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		return callHyundaiRest(url, HttpMethod.POST, param, headers, clazz);
	}
	
	public <T> T getHyundai(String url, String accessToken, Class<T> clazz) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		System.out.println("Bearer " + accessToken);
		headers.add("Authorization", "Bearer " + accessToken);
		headers.setContentType(MediaType.APPLICATION_JSON);
		return callHyundaiRest(url, HttpMethod.GET, null, headers, clazz);
	}
	
	
	public static void main(String[] args) {
		
		
		TokenResponse token = new Gson().fromJson("{\"access_token\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaWQiOiI1YmIyYzcyMDlhOTk0MzA0MTk0MzZlYzQiLCJ1aWQiOiIzMWYwMTNhNy0zOTJkLTQwMTEtODQ0Ni1iNzBlYzI4MDcwNDciLCJzaWQiOiJkOWEwYzdmYy1hMjc4LTQ4MmUtOWIzZS1kNjljOWVkODIzNjQiLCJleHAiOjE1ODQ1OTk4OTgsImlhdCI6MTU4NDUxMzQ5OCwiaXNzIjoiYmx1ZWxpbmsifQ.Fz2tTdy4QpomQ1ZBfgYQwv8IOpfYn33en1sGcchFewXAtq8sFvZJRDdlT79ePIVT4cUChYhnQo-xwnrPBlkn1TG17-TPeJy6xdmKScncpI48ULWSjqc4f1YiW42R9DOc3Tt_8jRZUTPNPEAw2h1O1V1zaznZ05bsFWeetghCh5Hw1GzDUi_ay08cWChlGMe7aPcKBqkICrCT0cA4o7YrCXjoc5DGZpdW0ncNeB_2AbVXuMk-elCiBChBvagGuSL_3artr-DH-okeNlOvXL_f2lHqD5fjpWG-Q5n71DAbf3MRZDpIvrdEL1aN-ALzCl7O-d5qgnQBfx6SwniGBtvJdA\",\"token_type\":\"Bearer\",\"refresh_token\":\"2SMAMYNUVT-GPX7YQWRRTA\",\"expires_in\":86400}"
				 				, TokenResponse.class);
		
		System.out.println(token);
		
	}
}
