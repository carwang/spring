package kr.co.ahope.common.web.mvc.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonAPIService {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public List<String> asList(String[] args) {
		return new ArrayList<String>(Arrays.asList(args));
	}

}
