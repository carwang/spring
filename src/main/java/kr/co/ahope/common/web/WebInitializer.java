package kr.co.ahope.common.web;

import java.util.Properties;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;

import kr.co.ahope.common.web.spring.config.DBContextConfig;
import kr.co.ahope.common.web.spring.config.RootContextConfig;
import kr.co.ahope.common.web.spring.config.ServletContextConfig;
import kr.co.ahope.common.web.spring.properties.PropertiesLoader;

public class WebInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext container) throws ServletException {

		//filter
		addFilter(container);
		
		//TODO - test monitoring
		/*
		container.addListener(new SessionListener());
		*/
		
		//RootContext
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(RootContextConfig.class);
		rootContext.register(DBContextConfig.class);
		container.addListener(new ContextLoaderListener(rootContext));
		
		//project active mode setting
		Properties prop = PropertiesLoader.load("spring.properties");
		rootContext.getEnvironment().setActiveProfiles(prop.getProperty("spring.active", "local"));

		//dipatcherSelvet
		addDsipatcherServlet(container);
	}
	
	private void addFilter(ServletContext container) {

		/*
		//TODO - test monitoring
		FilterRegistration.Dynamic monitoringFilter = container.addFilter("monitoringFilter", new MonitoringFilter());
		monitoringFilter.addMappingForUrlPatterns(null, false, "/*");
		*/
		
		//container.addFilter("encodingFilter", new CharacterEncodingFilter());
		//encoding filter
		FilterRegistration.Dynamic encodingfilter = container.addFilter("encodingFilter", new CharacterEncodingFilter());
		encodingfilter.setInitParameter("encoding", "UTF-8");
		encodingfilter.addMappingForUrlPatterns(null, false, "/*");
		
		//hidden method filter - to restful method
		FilterRegistration.Dynamic hiddenHttpMethodFilter = container.addFilter("hiddenHttpMethodFilter", new HiddenHttpMethodFilter());
		hiddenHttpMethodFilter.addMappingForUrlPatterns(null, false, "/*");
		
		/*
		FilterRegistration.Dynamic sitemeshFilter = container.addFilter("sitemeshFilter", new ConfigurableSiteMeshFilter());
		sitemeshFilter.addMappingForUrlPatterns(null, false, "/*");
		*/
		
	}
	
	private void addDsipatcherServlet(ServletContext container) {
		
		//Dispatacher Servlet Context
		AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
		dispatcherContext.register(ServletContextConfig.class);
		
		ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(dispatcherContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}

}
