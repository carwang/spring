package kr.co.ahope.common.web.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import kr.co.ahope.common.bean.APIResponse;
import kr.co.ahope.common.bean.ResultMap;
import kr.co.ahope.common.exception.LogicException;
import kr.co.ahope.common.exception.ResourceNotFoundException;
import kr.co.ahope.common.exception.RuntimeLogicException;

public class CommonAPIController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected String JSON_VIEW = "jsonView";
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody APIResponse exceptionHandler(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		
		logger.error("Error URI : " + req.getRequestURI());
		ex.printStackTrace();
		
		String errName 	= ex.getClass().getName();
		String err 	= "500";
		String errMsg = null;
		int httpStatus = 500;
		
		//TODO 에러 처리
		if(ex instanceof LogicException) {
			err = ((LogicException)ex).getCode();
			errMsg = ((LogicException)ex).getMessage();
		} else if (ex instanceof RuntimeLogicException) {
			err = ((RuntimeLogicException)ex).getCode();
			errMsg = ((RuntimeLogicException)ex).getMessage();
		} else if (ex instanceof MissingServletRequestParameterException) {
			httpStatus = 406;
			err = "406";
			errMsg = ((MissingServletRequestParameterException)ex).getMessage();
		} else if (ex instanceof HttpClientErrorException) {
			httpStatus = ((HttpClientErrorException) ex).getStatusCode().value();
			err = ((HttpClientErrorException) ex).getStatusText();
			errMsg = ((HttpClientErrorException) ex).getMessage();
		} else {
			err = "500";
			errMsg = "SERVER_ERROR [" + ex.getClass().getName() + "] : " + ex.getMessage();
		}
		
		res.setStatus(httpStatus);
		logger.error("["+errName+"] ["+err+"] ["+errMsg+"]");
		
		return new APIResponse(err, errMsg);
	}
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ModelAndView notFoundExceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex) {
		ResultMap resultMap = new ResultMap();
		resultMap.setStatus("404");
		resultMap.setMsg("Page not found");
		
		ModelAndView mv = new ModelAndView("redirect:/admin/error/404");
		return mv;
	}
}
