package kr.co.ahope.common.web.spring.resolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONValue;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import kr.co.ahope.common.bean.ParamMap;
import kr.co.ahope.common.exception.RuntimeLogicException;
import kr.co.ahope.common.util.CommonUtil;
import kr.co.ahope.common.web.spring.annotation.RequestJson;

public class RequestJsonArgumentResolver implements HandlerMethodArgumentResolver {
	
	@SuppressWarnings("unchecked")
	@Override
	public Object resolveArgument(MethodParameter param,
			ModelAndViewContainer mnv, NativeWebRequest request,
			WebDataBinderFactory beanFactory) throws Exception {
		
		Annotation[] paramAnns = param.getParameterAnnotations();  
		Class<?> paramType = param.getParameterType();  
		   
        for (Annotation paramAnn : paramAnns) {  
            if (RequestJson.class.isInstance(paramAnn)) {  
            	
            	RequestJson reqAttr = (RequestJson) paramAnn;  
            	HttpServletRequest httprequest = (HttpServletRequest) request.getNativeRequest();
            	
            	Object result = null;
            	
            	String jsonStr = getRequestBody(httprequest);
            	
				
            	if(reqAttr.required() && CommonUtil.isNull(jsonStr)) {
            		throw new RuntimeLogicException("Required Request Boby Json String Parameter is Null", "001");
            	}
            	
            	if(ParamMap.class.isAssignableFrom(paramType)){
            		ParamMap paramMap = new ParamMap(httprequest);
            		paramMap.putAll((HashMap<String, Object>)JSONValue.parse(jsonStr));
            		result = paramMap;
            		
            		if(reqAttr.requiredValues().length > 0) {
            			for(String requiredValue : reqAttr.requiredValues()) {
            				if(CommonUtil.isNull(paramMap.get(requiredValue))){
            					throw new RuntimeLogicException("MISSING_REQUIRED_VALUE : ["+requiredValue+"]", "334");
            				}
            			}
            		}
        		} else if(Map.class.isAssignableFrom(paramType)){
            		Map<String, Object> attrMap = (paramType.isAssignableFrom(Map.class)) ? new HashMap<String, Object>() : (Map<String, Object>)paramType.getDeclaredConstructor().newInstance();
            		attrMap.putAll((HashMap<String, Object>)JSONValue.parse(jsonStr));
            		result = attrMap;
            		
            		if(reqAttr.requiredValues().length > 0) {
            			for(String requiredValue : reqAttr.requiredValues()) {
            				if(CommonUtil.isNull(attrMap.get(requiredValue))){
            					throw new RuntimeLogicException("MISSING_REQUIRED_VALUE : ["+requiredValue+"]", "001");
            				}
            			}
            		}
            	} else if(List.class.isAssignableFrom(paramType)){
            		result = (List<?>)JSONValue.parse(jsonStr);
            	} else if(String.class.isAssignableFrom(paramType)){
            		result = jsonStr;
            	} else {
            		result = WebArgumentResolver.UNRESOLVED;
            	}
            	
                return result;
            }  
        }  
		
		return WebArgumentResolver.UNRESOLVED;
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(RequestJson.class);
	}
	
	public String getRequestBody(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = request.getReader();
			String line = "";
			while ((line = reader.readLine()) != null){
				sb.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
}
