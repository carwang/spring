
package kr.co.ahope.common.bean;

import lombok.Data;

@Data
public class FileInfo {
	
	private String fileName;
	private String fileSize;
	private String filePath;
}
