package kr.co.ahope.common.bean;

import lombok.Data;

@Data
public class HyundaiError {

	private String errCode;
	private String errMsg;
	private String errId;
	
}
