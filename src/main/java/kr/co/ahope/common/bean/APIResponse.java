package kr.co.ahope.common.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class APIResponse {

	private String status;
	private String msg;
	@JsonInclude(Include.NON_NULL)
	private Object data;
	
	public APIResponse() {
		this.status = "200";
		this.msg = "success";
	}
	public APIResponse(String status, String msg) {
		this.status = status;
		this.msg = msg;
	}
	public APIResponse(Object obj) {
		this.status = "200";
		this.msg = "success";
		this.data = obj;
	}
}
