package kr.co.ahope.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	
	public static String getNowDateStr() {
		return getDateStr("yyyy-MM-dd HH:mm:ss.SSS", new Date());
	}
	
	public static String getNowDateStr(String format) {
		return getDateStr(format, new Date());
	}
	
	public static String getDateStr(String format, Date date) {
		SimpleDateFormat sf = new SimpleDateFormat(format);
		sf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		return sf.format(date);
	}
	
	public static Date addDate(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, amount);
		return cal.getTime();
	}
	
	public static Date addHour(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, amount);
		return cal.getTime();
	}
}
