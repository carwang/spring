package kr.co.ahope.common.util;

import java.security.MessageDigest;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Hash {

	public static String sha256(String hashData) {
		
		String result = "";
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(hashData.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer(); 
			for(int i=0; i<byteData.length; i++) {
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			result = sb.toString();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static String sha256HMacHexString(String key, String hashData) {
		String hexString = "";
		try {
			Mac sha256HMAC = Mac.getInstance("HmacSHA256");
			SecretKey secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
			sha256HMAC.init(secretKey);
			sha256HMAC.update(hashData.getBytes("UTF-8"));
			hexString = byteToHexString(sha256HMAC.doFinal()); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hexString;
	}
	
	public static String byteToHexString(byte[] bytes) {
    	StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
    }
}
