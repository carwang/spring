package kr.co.ahope.common.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class FileUtil {

	public static void resizeImage(String originPath, String imgNewPath) {
		
		/*String imgOriginPath = originPath;
		String imgOriginName = imgOriginPath.substring(originPath.lastIndexOf(File.separator), imgOriginPath.length());
		int idx = originPath.lastIndexOf(".") + 1;
		String imgNewPath = "C:\\cums\\thumbnail" + File.separator + imgOriginName;
		String imgFormat = imgOriginPath.substring(idx, imgOriginPath.length());*/
		String imgOriginPath = originPath;
		int idx = originPath.lastIndexOf(".") + 1;
		String imgFormat = imgOriginPath.substring(idx, imgOriginPath.length());
		
		
		int newWidth = 600; // resize할 파일 가로 최대 길이 (비율에 맞게 조절됨)
		int newHeight = 500; // resize할 파일 세로 최대 길이 (비율에 맞게 조절됨)
		
		Image image;
		int imageWidth;
		int imageHeight;

		double ratio;
		int w;
		int h;
		
		try {
			
			image = ImageIO.read(new File(imgOriginPath));
			imageWidth = image.getWidth(null);
			imageHeight = image.getHeight(null);
			String imageResizeType = imageWidth < imageHeight ? "H":"W"; // W:넓이기준 H:높이기준
				
			if(imageResizeType.equals("W")){    // 넓이기준
                ratio = (double)newWidth/(double)imageWidth;
                w = (int)(imageWidth * ratio);
                h = (int)(imageHeight * ratio);
 
            } else if(imageResizeType.equals("H")){ // 높이기준
                ratio = (double)newHeight/(double)imageHeight;
                w = (int)(imageWidth * ratio);
                h = (int)(imageHeight * ratio);
 
            } else{ //설정값 (비율무시)
                w = newWidth;
                h = newHeight;
            }
			
			Image resizeImage = image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
			BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			Graphics g = newImage.getGraphics();
			g.drawImage(resizeImage, 0, 0, null);
			g.dispose();
			ImageIO.write(newImage, imgFormat, new File(imgNewPath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
}
