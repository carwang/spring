package kr.co.ahope.common.util;

import java.util.Map;

public class Validator {
	
	public static String nvl(String str, String dafault) {
		if(CommonUtil.isNull(str)) return dafault;
		else return str;
	}
	
	
	public static boolean isUnderLength(String value, int length) {
		if (value.length() > Integer.MAX_VALUE) {
			return false;
		}
		return (value.length() <= length) ? true : false;
	}
	
	public static boolean isUnderLength(int value, int length) {
		if (value > Integer.MAX_VALUE) {
			return false;
		}
		return isUnderLength(Integer.valueOf(value).toString(), length);
	}
	
	public static boolean isUnderLength(double value, int length) {
		if (value > Double.MAX_VALUE) {
			return false;
		}
		return isUnderLength(Double.valueOf(value).toString(), length);
	}
	
	public static boolean isUnderLength(float value, int length) {
		if (value > Float.MAX_VALUE) {
			return false;
		}
		return isUnderLength(Float.valueOf(value).toString(), length);
	}
	
	
	public static boolean isNullOrEmptyOrWhiteSpace(String value) {
		if (value == null || value.length() == 0) return true;
		for(char ch : value.toCharArray()) {
			if (!Character.isWhitespace(ch)) return false;
		}
        return true;
	}
	
	public static boolean isNullOrEmptyOrWhiteSpace(Map<?, ?> param) {
		if (!CommonUtil.isNull(param)) {
			for (Map.Entry<?, ?> entry : param.entrySet()) {
				if (entry.getValue() instanceof Map<?,?>) {
					for (Map.Entry<?, ?> innerEntry : ((Map<?, ?>)entry.getValue()).entrySet()) {
						return isNullOrEmptyOrWhiteSpace((String)innerEntry.getValue());
					}
				} else {
					return isNullOrEmptyOrWhiteSpace((String)entry.getValue());
				}
			}
		}
		return false;
	}
	
	public static boolean isEnglish(String value) {
		if (CommonUtil.isNull(value)) {
			return false;
		}
		return value.matches("[A-Za-z]+");
	}
	
	public static boolean isKorean(String value) {
		if (CommonUtil.isNull(value)) {
			return false;
		}
		return value.matches("[\u3131-\u3163\uac00-\ud7a3]+");
	}
	
	public static boolean isNumeric(String value) {
		if (CommonUtil.isNull(value)) {
			return false;
		}
		return value.matches("-?\\d+(\\.\\d+)?");
	}
	
	public static boolean isMobileCtn(String value) {
		if (!isNumOrDashes(value)) {
			return false;
		}
		return value.matches("01([0|1|6|7|8|9]?)-\\d{3,4}-\\d{4}");
	}

	public static boolean isCtn(String value) {
		if (!isNumOrDashes(value)) {
			return false;
		}
		return value.matches("0\\d{1,2}-\\d{3,4}-\\d{4}");
	}
	
	public static boolean isNumOrDashes(String value) {
		if (CommonUtil.isNull(value)) {
			return false;
		}
		return value.matches("[0-9-]+");
	}
	
	// Caution: There is not exist perfect email validation in the world..
	public static boolean isEmail(String value) {
		if (CommonUtil.isNull(value)) {
			return false;
		}
		return value.matches("^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"); 
	}
	
	public static Integer TryParseInt(String someText) {
	   try {
	      return Integer.parseInt(someText);
	   } catch (NumberFormatException ex) {
	      return null;
	   }
	}
}

