package kr.co.ahope.common.util;

public class Base16Encoder {
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String encode(byte[] bytes, int byteLen) {
	    char[] hexChars = new char[byteLen * 2];
	    for ( int j = 0; j < byteLen; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	public static String encode(byte[] bytes) {
		return encode(bytes, bytes.length);
	}

    public static byte[] decode(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
