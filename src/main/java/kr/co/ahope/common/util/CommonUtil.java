package kr.co.ahope.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import kr.co.ahope.common.exception.LogicException;

public class CommonUtil {

	public static boolean isNull(Object obj) {
		boolean result = false;
		try {
			if (obj == null) {
				throw new LogicException("Object empty");
			} else if (obj instanceof String && obj.equals("")) {
				throw new LogicException("String empty");
			} else if (obj instanceof Map && ((Map<?, ?>) obj).isEmpty()) {
				throw new LogicException("Map empty");
			} else if (obj instanceof Set && ((Set<?>) obj).isEmpty()) {
				throw new LogicException("Set empty");
			} else if (obj instanceof List) {
				List<?> list = (List<?>)obj;
				if(list.size() == 0) {
					throw new LogicException("List empty");
				} else {
					for(Object listObj: list) {
						if(isNull(listObj)) {
							result = true;
							break;
						}
					}
				}
			} 
		} catch (Exception e) {
			result = true;
		}
		
		return result;
	}
	
	public static String insertDashForCtn(String ctn) {
		if (ctn == null || ctn.length() != 11)
			return ctn;
		return ctn.substring(0, 3) + "-" + ctn.substring(3, 7) + "-" + ctn.subSequence(7, 11);
	}
	
	public static String nvl(String str, String dafault) {
		if(isNull(str)) return dafault;
		else return str;
	}
	
	public static String nvl(String str){
		return nvl(str, "");
	}

	public static int nvl(Integer str) {
		if(isNull(str)) return 0;
		else return str;
	}
	
	public static String makeZeroSpaceStr(int size, String str) {
    	String zero = "";
    	for(int i = str.length(); i < size; i++) {
    		zero += "0";
    	}
    	return zero + str;
    }
    
    public static String byteToHexString(byte[] bytes) {
    	StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
    }
    
    public static String getRandomStr() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
    }
    
    public static String getFilePath(String fileLocation) {
    	return fileLocation.substring(0, fileLocation.lastIndexOf("/"));
    }
 
    public static String getFileExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
    }
 
    public static String getFileName(String fileName) {
		return fileName.substring(fileName.lastIndexOf("/")+1);
    }
    
    public static String getNowDateStr(String format) {
		return getDateStr(format, new Date());
	}
	
	public static String getDateStr(String format, Date date) {
		return new SimpleDateFormat(format).format(date);
	}
	
	public static String changeDateFormat(String dateStr, String format1, String format2) {
		Date d = convertStrToDate(dateStr, format1);
		return getDateStr(format2, d);
	}
 	
	public static Date convertStrToDate(String dateStr, String format) {
		SimpleDateFormat sf = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = sf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static String getYesterday() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return getDateStr("yyyy-MM-dd", cal.getTime());
	}
	
    public static boolean removeDirectory(File path) {
		if (path.exists() == true) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory() == true) {
					removeDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}
    
    private static final String enterTimeString = "7:29";
    private static final String leaveTimeString = "23:59";
    
    private static final String TIME_FORMAT = "HH:mm";
    private static SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
    
    public static boolean isWokingTime() {
    	Calendar now = Calendar.getInstance();
		try {
			Date currentTime = sdf.parse(now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE));
			Date enterTime = sdf.parse(enterTimeString);
			Date leaveTime = sdf.parse(leaveTimeString);
			if (leaveTime.after(currentTime) && enterTime.before(currentTime)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
    }
    
    public static String[] splitCtn(String ctn) {
    	String[] result = null;
    	if (CommonUtil.isNull(ctn)) return result;
    	int length = ctn.length();
    	if (length < 10 || length > 11) return result;
    	int i = (length == 10) ? 6 : 7;
    	result = new String[3];
    	result[0] = ctn.substring(0, 3);
    	result[1] = ctn.substring(3, i);
    	result[2] = ctn.substring(i);
    	return result;
    }
    
    public static String generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        long randomNumber = Long.parseLong(new String(digits));
        return Long.toString(randomNumber);
    }
    
    public static boolean checkSqlDateValidation(String date) {    	
    	SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");    	
		try {
			dataFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}		
    	return true;
    }
    
    public static String convertNormalStringToSqlDate(String date) {    	
    	SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	String sqlDate = "";
    	try {
			java.sql.Date dt = new java.sql.Date(dataFormat.parse(date).getTime());
			sqlDate = dataFormat.format(dt);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    		
    	return sqlDate;
    }
    
    public static String convertNormalStringToSqlDate(String date, String dateFormat) {    	
    	SimpleDateFormat dataFormat = new SimpleDateFormat(dateFormat);
    	String sqlDate = "";
    	try {
			java.sql.Date dt = new java.sql.Date(dataFormat.parse(date).getTime());
			sqlDate = dataFormat.format(dt);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    		
    	return sqlDate;
    }
    
    public static long doDiffOfDate(String currentDate, String diffDate) {
    	long diffDays = 0;
	    try {
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	        Date cDate = formatter.parse(currentDate);
	        Date dDate = formatter.parse(diffDate);
	         
	        // 시간차이를 시간,분,초를 곱한 값으로 나누면 하루 단위가 나옴	        
	        long diff = cDate.getTime() - dDate.getTime();
	        diffDays = diff / (24 * 60 * 60 * 1000);	        
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }
	    //diffDate가 currentDate보다 미래면 음수, 아니면 양수 반환됨
	    return diffDays;
	}
    
    public static long doDiffOfDate(String currentDate, String diffDate, String dateFormat) {
    	long diffDays = 0;
	    try {
	        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
	        Date cDate = formatter.parse(currentDate);
	        Date dDate = formatter.parse(diffDate);
	         
	        // 시간차이를 시간,분,초를 곱한 값으로 나누면 하루 단위가 나옴	        
	        long diff = cDate.getTime() - dDate.getTime();
	        diffDays = diff / (24 * 60 * 60 * 1000);	        
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }
	    //diffDate가 currentDate보다 미래면 음수, 아니면 양수 반환됨
	    return diffDays;
	}
    
    public static String getCurrentTime() {
    	Calendar calendar = Calendar.getInstance();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	return formatter.format(calendar.getTime());
    }
    
    public static String getCurrentTime(String dataFormat) {
    	Calendar calendar = Calendar.getInstance();
    	SimpleDateFormat formatter = new SimpleDateFormat(dataFormat);
    	return formatter.format(calendar.getTime());
    }
    
    public static String maskingCtn(String ctn) {
		if (ctn != null) {
			if (ctn.length() > 4) {
				StringBuilder maskingParam = new StringBuilder();
				for (int i = 0; i < ctn.length()-4; i++) {
					maskingParam.append('*');
				}
				return maskingParam+ctn.substring(ctn.length()-4, ctn.length());
			} else {
				return ctn;
			}
		} else {
			return "";
		}
	}
    
    public static boolean convertStringYnToBoolean(String yOrN) {
    	if (isNull(yOrN)) return false;
    	return yOrN.equals("Y");
    }
    
    public static int convertStringToInt(String num) {
    	if (isNull(num)) 
    		num = "0";
    	else if (!num.matches("[0-9]+"))
    		num = "0";
    	return Integer.parseInt(num); 
    }
    
    public static String pullNumber(String text) {
    	String num = "";
    	for (int i = 0; i < text.length(); i++) {
    		char c = text.charAt(i);
    		if (Character.isDigit(c))
    			num += c;
    	}
    	return num;
    }
    
	public static String insertCommaBetweenNumbers(String number) {
		String ret = "";
		if (number.matches("[0-9]+")) {
			for (int i = 0; i < number.length(); i++) {
				if (i != 0)
					ret += ",";
				ret += Character.toString(number.charAt(i));
			}
		} else {
			ret = number;
		}
		return ret;
	}
    public static String convertDashToSpace(String text) {
    	return text.replaceAll("-", " ");
    }
    
    static boolean isEmailChar(char c) {
		return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('0' <= c || c <= '9') || c == '@' || c == '-' || c == '.';
	}
	static String removeWhitespaceForEmail(String text) {
		String result = "";
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (c == ' ') {
				Character beforeC = null;
				Character afterC = null;
				if (i > 0)
					beforeC = text.charAt(i - 1);
				if (i < text.length() - 1)
					afterC = text.charAt(i + 1);
				if (beforeC == null || afterC == null)
					continue;
				if ((isEmailChar(beforeC) || beforeC == ' ') &&
						(isEmailChar(afterC) || afterC == ' ')) {
					continue;
				}
			}
			result += c;
		}
		return result;
	}
    
    public static String get(String urlStr) {
		String ret = null;
		
		BufferedReader br = null;
		
		try {
			URL url = new URL(urlStr);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
	
			con.setRequestMethod("GET");
			br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
			
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = br.readLine()) != null) {
				result.append(line);
			}
			ret = result.toString();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(br != null) try { br.close(); } catch(Exception e) {}
		}
		
		return ret;
	}
	
	public static String post(String urlStr, String param) {
		String ret = null;
		BufferedReader br = null;

		try {
			URL url = new URL(urlStr);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			
			OutputStream outputStream = con.getOutputStream();
			outputStream.write(param.getBytes("UTF-8"));
			outputStream.flush();
			outputStream.close();
			
			br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
	
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = br.readLine()) != null) {
				result.append(line);
			}
			ret = result.toString();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(br != null) try { br.close(); } catch(Exception e) {}
		}
		
		return ret;
	}
	
	
	public static String utf8ToEucKr(String text) {
		String str = "";
		try {
			str = (new String(text.getBytes("utf-8"),"euc-kr")).trim();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	public static String eucKrToUtf8(String text) {
		String str = "";
		try {
			str = (new String(text.getBytes("euc-kr"),"utf-8")).trim();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	// 클래스의 모든 맴버 변수를 출력
	public static String GetAllMemberVariablesForString(Object obj) {
		  StringBuilder result = new StringBuilder();
		  String newLine = System.getProperty("line.separator");

		  result.append( obj.getClass().getName() );
		  result.append( " Object {" );
		  result.append(newLine);

		  //determine fields declared in this class only (no fields of superclass)
		  Field[] fields = obj.getClass().getDeclaredFields();

		  //print field names paired with their values
		  for ( Field field : fields  ) {
		    result.append("  ");
		    try {
		      result.append( field.getName() );
		      result.append(": ");
		      //requires access to private field:
		      result.append( field.get(obj) );
		    } catch ( IllegalAccessException ex ) {
		      	ex.printStackTrace();
		    }
		    result.append(newLine);
		  }
		  result.append("}");

		  return result.toString();
	}
	
	public static String getRandomNumber(int length) {
		String result = "";
		
		Random generator = new Random();	
		
		for (int i=0; i<length;i++) {
			result += String.valueOf(generator.nextInt(10));
		}
		return result;
	}
	
	public static String generateRandomFileName(){
		return new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date())+"_"+getRandomNumber(5);
	}
	
	public static String getFirstDate(String format) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return new SimpleDateFormat(format).format(cal.getTime());
	}
	 
	public static String hashPassword(String s) {
		return Base64Encoder.encodeString(Hash.sha256(s));
	}
	
	public static Double conversionToUnit(Double d, String unit) {
		if (d == 0) {
			return d;
		}
		
		switch (unit) {
		case "mmAq":
				d = d * 10000.0005;
			break;
		case "kPa":
				d =d * 98.0665; 
			break;
		case "mbar":
				d = d * 980.665;
			break;
		case "bar":
				d = d * 0.980665;
			break;
		case "mmH2o":
				d = d * 10000.000500;
			break;
		case "psi":
				d = d * 14.223393;
			break;
		case "℉":
				d = d * 1.8 + 32; 
			break;
		case "CMH":
				d = d * 60;
			break;
		}
		return d;
	}
}
