package kr.co.ahope.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String code;
	private String msg;
	
	public BadRequestException() {
		super();
	}
	
	public BadRequestException(String msg) {
		this.msg = msg;
	}
	
	public BadRequestException(String msg, String code) {
		this.code = code;
		this.msg = msg;
	}
	
	@Override
	public String getMessage() {
		return this.msg;
	}
	
	public String getCode() {
		return this.code;
	}
}
