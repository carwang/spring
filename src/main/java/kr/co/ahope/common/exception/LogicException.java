package kr.co.ahope.common.exception;

public class LogicException extends Exception {

	private static final long serialVersionUID = -4124980788043425818L;
	
	private int status = 500;
	private String	code;
	private String	msg;

	public LogicException(String code) {
		this.code 	= code;
	}

	public LogicException(String code, String msg) {
		this.code	= code;
		this.msg	= msg;
	}
	
	public LogicException(int status, String code, String msg) {
		this.status = status;
		this.code	= code;
		this.msg	= msg;
	}

	@Override
	public String getMessage() {
		return this.msg;
	}

	public String getCode() {
		return this.code;
	}
	
	public int getStatus() {
		return this.status;
	}
	
	public void setMessage(String msg) {
		this.msg = msg;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
}
