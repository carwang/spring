package kr.co.ahope.carwang.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.ahope.carwang.batch.car.service.CarBatchService;
import kr.co.ahope.carwang.batch.user.service.UserBatchService;

@Component
public class CronTable {
	
	@Autowired
	private CarBatchService carBatchservice;
	
	@Autowired
	private UserBatchService userBatchservice;

	@Scheduled(cron="0 0 * * * *")
	public void test() throws Exception {
		carBatchservice.updateUserCarHistory();
		carBatchservice.updateCarRank();
		userBatchservice.updateAccessToken();
	}

}
