package kr.co.ahope.carwang.batch.car.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.ahope.carwang.api.car.bean.Car;
import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.batch.car.bean.CarHistory;
import kr.co.ahope.common.web.mvc.dao.CommonDAO;

@Repository
public class CarBatchDAO extends CommonDAO {

	public int getUserListCount() {
		return selectOne("batch.car.getUserListCount");
	}
	
	public List<User> getUserList(int startIndex) {
		return selectList("batch.car.getUserList", startIndex);
	}	
	
	public List<Car> getUserCarList(Long userSeq) {
		return selectList("batch.car.getUserCarList", userSeq);
	}

	public CarHistory getLastCarHistory(Long carSeq) {
		return selectOne("batch.car.getLastCarHistory", carSeq);
	}

	public int insertCarHistory(CarHistory carHistory) {
		return insert("batch.car.insertCarHistory", carHistory); 
	}

	public Double getFuelEfficiencyAverage(Long carSeq) {
		return selectOne("batch.car.getFuelEfficiencyAverage", carSeq);
	}

	public int updateUserCar(Car car) {
		return update("batch.car.updateUserCar", car);
	}

	public int getCarRankListCount() {
		return selectOne("batch.car.getCarRankListCount");
	}
	
	public List<Car> getCarRankList(int startIndex) {
		return selectList("batch.car.getCarRankList");
	}
	
	public int updateCarRank(Car car) {
		return update("batch.car.updateCarRank", car);
	}

}
