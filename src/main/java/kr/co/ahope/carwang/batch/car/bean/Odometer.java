package kr.co.ahope.carwang.batch.car.bean;

import lombok.Data;

@Data
public class Odometer {
	
	private String timestamp;
	private Double value;
	private Integer unit;
}
