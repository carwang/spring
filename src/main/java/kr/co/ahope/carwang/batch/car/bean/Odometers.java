package kr.co.ahope.carwang.batch.car.bean;

import java.util.List;

import lombok.Data;

@Data
public class Odometers {

	private List<Odometer> odometers;
	private String msgId;

}
