package kr.co.ahope.carwang.batch.car.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ahope.carwang.api.car.bean.Car;
import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.batch.car.bean.CarHistory;
import kr.co.ahope.carwang.batch.car.bean.DTE;
import kr.co.ahope.carwang.batch.car.bean.Odometer;
import kr.co.ahope.carwang.batch.car.bean.Odometers;
import kr.co.ahope.carwang.batch.car.dao.CarBatchDAO;
import kr.co.ahope.common.web.client.HyundaiRestTemplate;

@Service
public class CarBatchService {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${api.host}")
	private String API_HOST; 
	
	@Autowired
	private CarBatchDAO dao;
	
	@Autowired
	private HyundaiRestTemplate hyundaiRestTemplate;
	
	public void updateUserCarHistory() {
		
		int userCount = dao.getUserListCount();
		if(userCount == 0) return;
		
		int startIndex = 0;
		do {
			
			List<User> userList = dao.getUserList(startIndex);
			
			for (User user : userList) {
				
				List<Car> carList = dao.getUserCarList(user.getUserSeq());
				
				for(Car car : carList) {
					
					try {
						//setp 1 연비 정보 갱신
						//step 1.1 현재 주행가능 거리 / 누적 주행 거리 정보 조회
						DTE dte = hyundaiRestTemplate.getHyundai(API_HOST+"/api/v1/car/status/"+car.getCarId()+"/dte"
																, user.getAccessToken()
																, DTE.class);
						Odometer odometer = hyundaiRestTemplate.getHyundai(API_HOST+"/api/v1/car/status/"+car.getCarId()+"/odometer"
																			, user.getAccessToken()
																			, Odometers.class)
																.getOdometers()
																.get(0);
						
						//step 1.2 이전 연비 정보 조회
						CarHistory lastCarHistory = dao.getLastCarHistory(car.getCarSeq());
						
						//step 1.3 연비 계산
						//이전 주행 이력이 없을 경우 계산 할수 없기 때문에 정보만 쌓고 그냥 패스
						double fuelEfficiency = 0;
						if (lastCarHistory != null) {
							double dteValue = dte.getValue();
							double odometerValue = odometer.getValue();
							
							double lastDteValue = lastCarHistory.getDte();
							double lastOdometerValue = lastCarHistory.getOdometer();
							/*
							- 주행 가능 거리가 늘어났을 경우 주유를 했다고 판단하고
							연비 정보를 갱신하지 않고 그냥 이전 정보로 냅두고 다음 차수부터 갱신.
							- 주행 정보가 변경 없을 경우에도 계산 할수 없기 때문에 정보만 쌓고 그냥 패스
							 */
							if (dteValue > lastDteValue || odometerValue == lastOdometerValue) {
								fuelEfficiency = lastCarHistory.getFuelEfficiency();
							} else {
								fuelEfficiency = Math.round(((odometerValue - lastOdometerValue) * 100 / (lastDteValue - dteValue))*100)/100.0;  
							}
						}
						
						//step 2 이력 정보 쌓기
						CarHistory carHistory = new CarHistory();
						carHistory.setCarSeq(car.getCarSeq());
						carHistory.setFuelEfficiency(fuelEfficiency);
						carHistory.setDte(dte.getValue());
						carHistory.setDteUnit(dte.getUnit());
						carHistory.setOdometer(odometer.getValue());
						carHistory.setOdometerUnit(odometer.getUnit());
						
						dao.insertCarHistory(carHistory);
						
						//step 3 차량 정보 갱신
						//최근 이력 12개의 연비 평균값으로 계산
						car.setFuelEfficiency(dao.getFuelEfficiencyAverage(car.getCarSeq()));
						car.setOdometer(odometer.getValue());
						car.setOdometerUnit(odometer.getUnit());
						
						dao.updateUserCar(car);
						
					} catch (Exception e) {
						e.printStackTrace(); //TODO
						logger.error(e.getMessage());
					}

				}
			}
			startIndex += 1000;
		} while(startIndex < userCount);
		
	}
	
	//랭킹 정보 수정
	public void updateCarRank() {
		int carCount = dao.getCarRankListCount();
		if(carCount == 0) return;
		
		int startIndex = 0;
		do {
			List<Car> carList = dao.getCarRankList(startIndex);
			for (Car car : carList) {
				dao.updateCarRank(car);
			}
			startIndex += 1000;
		} while(startIndex < carCount);
	}
}
