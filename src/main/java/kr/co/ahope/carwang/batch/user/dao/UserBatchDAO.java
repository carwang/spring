package kr.co.ahope.carwang.batch.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.common.web.mvc.dao.CommonDAO;

@Repository
public class UserBatchDAO extends CommonDAO {

	public int getUserListCount() {
		return selectOne("batch.user.getUserListCount"); 
	}
	
	public List<User> getUserList(int startIndex) {
		return selectList("batch.user.getUserList", startIndex); 
	}
	
	public int updateUser(User user) {
		return update("batch.user.updateUser", user); 
	}
	

}
