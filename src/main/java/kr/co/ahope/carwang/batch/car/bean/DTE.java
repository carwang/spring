package kr.co.ahope.carwang.batch.car.bean;

import lombok.Data;

@Data
public class DTE {

	private String timestamp;
	private Double value;
	private Integer unit;
	private String msgId;
	
}
