package kr.co.ahope.carwang.batch.car.bean;

import lombok.Data;

@Data
public class CarHistory {

	private Long historySeq;
	private String historyDatetime;
	private Long carSeq;
	private Double fuelEfficiency;
	private Double dte;
	private int dteUnit;
	private Double odometer; 
	private int odometerUnit;
	
}
