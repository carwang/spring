package kr.co.ahope.carwang.batch.user.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ahope.carwang.api.user.bean.TokenResponse;
import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.batch.user.dao.UserBatchDAO;
import kr.co.ahope.common.web.client.HyundaiRestTemplate;

@Service
public class UserBatchService {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HyundaiRestTemplate hyundaiRestTemplate;
	
	@Value("${api.host}")
	private String API_HOST; 

	@Value("${api.client.id}")
	private String API_CLIENT_ID;
	
	@Value("${api.client.secret}")
	private String API_CLIENT_SECRET;
	
	@Value("${redirect.uri.user}")
	private String REDIRECT_URI_USER;
	
	@Autowired
	private UserBatchDAO dao;
	
	
	public void updateAccessToken() throws Exception {
		
		int userCount = dao.getUserListCount();
		if(userCount == 0) return;
		
		int startIndex = 0;			
		do {
			List<User> userList = dao.getUserList(startIndex);
			for (User user : userList) {
				//token 갱신
				String url = API_HOST + "/api/v1/user/oauth2/token";
				String paramStr = "grant_type=refresh_token" //갱신
									+"&refresh_token="+user.getRefreshToken()
									+"&redirect_uri="+REDIRECT_URI_USER;
				TokenResponse token = hyundaiRestTemplate.postHyundaiToken(url, API_CLIENT_ID, API_CLIENT_SECRET, paramStr, TokenResponse.class); 

				user.setAccessToken(token.getAccessToken());
				dao.updateUser(user);
			}
			startIndex += 1000;
		} while(startIndex < userCount);
	}
	
}
