package kr.co.ahope.carwang.api.car.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class Location {

	private String timestamp;
	private Float lat;
	private Float lon;
	private Float alt;
	private Integer type;
	
	@JsonInclude(Include.NON_NULL)
	private String msgId;
	
}
