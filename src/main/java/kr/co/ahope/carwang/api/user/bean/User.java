package kr.co.ahope.carwang.api.user.bean;

import lombok.Data;

@Data
public class User {

	private Long userSeq;
	private String userId;
	private String apiToken;
	private String accessToken;
	private String refreshToken;
	private Boolean carServiceAgreement;
	
}
