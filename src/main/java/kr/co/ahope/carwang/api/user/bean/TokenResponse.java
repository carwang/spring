package kr.co.ahope.carwang.api.user.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.annotations.SerializedName;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TokenResponse {

	//status 200
	
	@SerializedName("access_token")
	@JsonInclude(Include.NON_NULL)
	private String accessToken;

	@SerializedName("refresh_token")
	@JsonInclude(Include.NON_NULL)
	private String refreshToken;

	@SerializedName("token_type")
	@JsonInclude(Include.NON_NULL)
	private String tokenType;

	@SerializedName("expires_in")
	@JsonInclude(Include.NON_NULL)
	private Integer expiresIn;
	
}
