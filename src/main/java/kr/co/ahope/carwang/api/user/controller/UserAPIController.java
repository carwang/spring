package kr.co.ahope.carwang.api.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.api.user.service.UserAPIService;
import kr.co.ahope.common.exception.LogicException;
import kr.co.ahope.common.web.mvc.controller.CommonAPIController;

@Controller
public class UserAPIController extends CommonAPIController {

	@Autowired
	private UserAPIService service;
	
	@Value("${api.host}")
	private String API_HOST;
	
	@Value("${api.client.id}")
	private String API_CLIENT_ID;
	
	@Value("${api.client.secret}")
	private String API_CLIENT_SECRET;
	
	@Value("${redirect.uri.user}")
	private String REDIRECT_URI_USER;
	
	@Value("${deeplink.uri}")
	private String DEEPLINK_URI;
	

	@GetMapping("/api/user/login")
	public String login() {
		
		//TODO
		String redirectUri = API_HOST + "/api/v1/user/oauth2/authorize" 
							+ "?response_type=code"
							+ "&state=ahope"
							+ "&client_id=" + API_CLIENT_ID
							+ "&redirect_uri=" + REDIRECT_URI_USER;
		
		return "redirect:" + redirectUri;
	}
	
	@GetMapping("/api/user/auth")
	public String getAgreement(@RequestParam(value = "code", required = false) String code
								, @RequestParam(value = "state", required = false) String state
								, Model model) throws Exception {
		String returnStr = "";
		
		if(!state.equals("ahope")) {
			//TODO
			throw new LogicException(410, "401", "Unauthorized. Return status value is wrong");
		}
		
		User user = service.auth(code);
		
		//제 3자 동의 여부 확인
		if(user.getCarServiceAgreement() != null && user.getCarServiceAgreement()) {
			returnStr =  "redirect:" + DEEPLINK_URI + "?apiToken=" + user.getApiToken();
		} else {
			model.addAttribute("user", user);
			model.addAttribute("agreementUrl", API_HOST + "/api/v1/car-service/agreement");
			returnStr = "/api/agreementSubmit";
		}
		
		return returnStr;
	}
	
	
}
