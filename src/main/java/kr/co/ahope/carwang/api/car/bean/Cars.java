package kr.co.ahope.carwang.api.car.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Cars {

	private List<Car> cars;
	private String msgId;
	
	public Cars() {
		this.cars = new ArrayList<Car>();
	}
}
