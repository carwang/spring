package kr.co.ahope.carwang.api.car.bean;

import lombok.Data;

@Data
public class LocationResponse {

	private String requestId;
	private String msgId;
	
}
