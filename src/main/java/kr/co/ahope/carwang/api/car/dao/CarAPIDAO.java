package kr.co.ahope.carwang.api.car.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.ahope.carwang.api.car.bean.Car;
import kr.co.ahope.carwang.api.car.bean.CarLocation;
import kr.co.ahope.common.web.mvc.dao.CommonDAO;

@Repository
public class CarAPIDAO extends CommonDAO {

	public int upsertCar(Car car) {
		return insert("api.car.upsertCar", car);
	}
	
	public Car getCar(Car car) {
		return selectOne("api.car.getCar", car);
	}
	/*
	public List<Car> getCarList(Long userSeq) {
		return selectList("api.car.getCarList", userSeq);
	}*/

	public List<Car> getFuelEfficiencyTopList() {
		return selectList("api.car.getFuelEfficiencyTopList");
	}

	public List<Car> getOdometerTopList() {
		return selectList("api.car.getOdometerTopList");
	}

	public int updateCarLocationWithCarId(CarLocation carLocation) {
		return update("api.car.updateCarLocationWithCarId", carLocation);
	}
	
	public CarLocation getCarLocationWithCarId(String carId) {
		return selectOne("api.car.getCarLocationWithCarId", carId);
	}

	public String getOptionValue(String option) {
		return selectOne("api.car.getOptionValue", option);
	}
	
	public List<String> getCarIdList(Long userSeq) {
		return selectList("api.car.getCarIdList", userSeq);
	}
	
	public int deleteCars(List<String> delCarIdList) {
		return delete("api.car.deleteCars", delCarIdList);
	}

}
