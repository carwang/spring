package kr.co.ahope.carwang.api.user.bean;

import lombok.Data;

@Data
public class Agreement {
	
	private String userId;
	private String state;
	
}
