package kr.co.ahope.carwang.api.car.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.ahope.carwang.api.car.bean.Car;
import kr.co.ahope.carwang.api.car.service.CarAPIService;
import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.common.bean.APIResponse;
import kr.co.ahope.common.exception.LogicException;
import kr.co.ahope.common.web.mvc.controller.CommonAPIController;
import kr.co.ahope.common.web.spring.annotation.RequestAttribute;

@Controller
public class CarAPIController extends CommonAPIController {

	@Value("${deeplink.uri}")
	private String DEEPLINK_URI;
	
	@Autowired
	private CarAPIService service;
	
	@GetMapping("/api/car/agreement")
	public String agreement(@RequestParam(value = "userId", required = false) String userId
							, @RequestParam(value = "state", required = false) String state) throws Exception {

		if (!state.equals("ahope")) {
			//TODO 토큰 처리 해야함? 귀찮..
			throw new LogicException(410, "401", "Unauthorized. Return status value is wrong");
		}
		
		return "redirect:" + DEEPLINK_URI + "?apiToken=" + service.getAPIToken(userId);
	}
	
	@GetMapping("/api/cars")
	public @ResponseBody APIResponse getCarList(@RequestAttribute("user") User user) throws Exception {
		return new APIResponse(service.getCarList(user)); 
	}
	
	@GetMapping("/api/cars/{carId}")
	public @ResponseBody APIResponse getCar(@RequestAttribute("user") User user
							, @PathVariable("carId") String carId) throws Exception {
		
		Car car = new Car();
		car.setUserSeq(user.getUserSeq());
		car.setCarId(carId);
		
		return new APIResponse(service.getCar(car)); 
	}
	
	@GetMapping("/api/cars/{carId}/location")
	public @ResponseBody APIResponse getCarLocation(@RequestAttribute("user") User user
									, @PathVariable("carId") String carId) throws Exception {
		
		Car car = new Car();
		car.setUserSeq(user.getUserSeq());
		car.setCarId(carId);
		
		return new APIResponse(service.getCarLocation(user, car)); 
	}

	@GetMapping("/api/cars/top10")
	public @ResponseBody APIResponse getTopList(@RequestParam("type") Integer type) throws Exception {
		return new APIResponse(service.getTopList(type));
	}
	
}
