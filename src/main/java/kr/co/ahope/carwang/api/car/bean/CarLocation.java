package kr.co.ahope.carwang.api.car.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class CarLocation {
	
	@JsonIgnore
	private String carId;
	
	private Float lat;
	private Float lon;
	
	private Integer projectionType;
	private String locationTimestamp;

	private String apiKey;
	
}
