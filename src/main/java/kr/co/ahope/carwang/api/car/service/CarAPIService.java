package kr.co.ahope.carwang.api.car.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ahope.carwang.api.car.bean.Car;
import kr.co.ahope.carwang.api.car.bean.CarLocation;
import kr.co.ahope.carwang.api.car.bean.Cars;
import kr.co.ahope.carwang.api.car.bean.Location;
import kr.co.ahope.carwang.api.car.bean.LocationResponse;
import kr.co.ahope.carwang.api.car.dao.CarAPIDAO;
import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.api.user.dao.UserAPIDAO;
import kr.co.ahope.common.web.client.HyundaiRestTemplate;
import kr.co.ahope.common.web.mvc.service.CommonAPIService;

@Service
public class CarAPIService extends CommonAPIService {

	@Value("${api.host}")
	private String API_HOST;
	
	@Value("${map.type}")
	private String MAP_TYPE;
	
	@Autowired
	private HyundaiRestTemplate hyundaiRestTempalte;
	
	@Autowired
	private CarAPIDAO dao;
	
	@Autowired
	private UserAPIDAO userDAO;
	
	public String getAPIToken(String userId) {
		userDAO.updateCarServiceAgreement(userId);
		return userDAO.getAPITokenWithUserId(userId);
	}
	
	public List<Car> getCarList(User user) throws Exception {
		//차량 목록 조회
		Cars cars = hyundaiRestTempalte.getHyundai(API_HOST + "/api/v1/car/profile/carlist", user.getAccessToken(), Cars.class);
		List<String> delCarIdList = dao.getCarIdList(user.getUserSeq());
		if (cars != null && cars.getCars() != null) {
			for (Car car : cars.getCars()) {
				//조회한 차량 정보 갱신
				car.setUserSeq(user.getUserSeq());
				dao.upsertCar(car);
				String carId = car.getCarId();
				if (delCarIdList.indexOf(carId) >= 0) {
					delCarIdList.remove(carId);
				}
			}
			if (delCarIdList.size() > 0) {
				dao.deleteCars(delCarIdList);			
			}
			
		} else {
			cars = new Cars();
		}
		
		return cars.getCars();
	}
	
	public Car getCar(Car car) {
		return dao.getCar(car);
	}
	
	public CarLocation getCarLocation(User user, Car car) {
		
		Location location = null;
		boolean locationFlag = true;
		
		try {
			//실시간 정보 요청
			LocationResponse response = hyundaiRestTempalte.getHyundai(API_HOST+"/api/v1/car-remote/status/"+car.getCarId()+"/location"
																		, user.getAccessToken()
																		, LocationResponse.class);
			//실시간 정보 요청 결과 조회
			location = hyundaiRestTempalte.getHyundai(API_HOST+"/api/v1/car-remote/status/response/"+car.getCarId()+"/"+response.getRequestId() 
														, user.getAccessToken()
														, Location.class);
		} catch(Exception e) {
			//TODO 
			e.printStackTrace();
			logger.error(e.getMessage());
			locationFlag = false;
		}
		
		if(!locationFlag) {
			try {
				//실시간 정보 요청 실패 시 최종 주차 위치 조회
				location = hyundaiRestTempalte.getHyundai(API_HOST+"/api/v1/car/status/"+car.getCarId()+"/parklocation"
															, user.getAccessToken()
															, Location.class);
			} catch(Exception e) {
				//TODO 
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		CarLocation carLocation;
		if(location != null) {
			//조회 한 정보를 DB에 저장
			carLocation = new CarLocation();
			carLocation.setLat(location.getLat());
			carLocation.setLon(location.getLon());
			carLocation.setProjectionType(location.getType());
			carLocation.setLocationTimestamp(location.getTimestamp());
			carLocation.setCarId(car.getCarId());
			dao.updateCarLocationWithCarId(carLocation);
		} else {
			//위치 정보 조회 실패 시 DB에 있는 정보로 전달
			carLocation = dao.getCarLocationWithCarId(car.getCarId());
		}
		
		//map에서 사용할 api key 조회
		carLocation.setApiKey(dao.getOptionValue(MAP_TYPE+"_map_key"));
		
		return carLocation;
	}

	public List<Car> getTopList(int type) {
		return (type == 0) ? dao.getFuelEfficiencyTopList() : dao.getOdometerTopList();
	}

}
