package kr.co.ahope.carwang.api.user.dao;

import org.springframework.stereotype.Repository;

import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.common.web.mvc.dao.CommonDAO;

@Repository
public class UserAPIDAO extends CommonDAO {

	public User getUser(String userId) {
		return selectOne("api.user.getUser", userId); 
	}
	
	public int upsertUser(User user) {
		return insert("api.user.upsertUser", user);
	}

	public Boolean checkCarServiceAgreement(User user) {
		return selectOne("api.user.checkCarServiceAgreement", user);
	}

	public User getUserWithAPIToken(String apiToken) {
		return selectOne("api.user.getUserWithAPIToken", apiToken);
	}
	
	public String getAPITokenWithUserId(String userId) {
		return selectOne("api.user.getAPITokenWithUserId", userId);
	}
	
	public int updateCarServiceAgreement(String userId) {
		return update("api.user.updateCarServiceAgreement", userId);
	}
	
}
