package kr.co.ahope.carwang.api.user.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ahope.carwang.api.user.bean.TokenResponse;
import kr.co.ahope.carwang.api.user.bean.User;
import kr.co.ahope.carwang.api.user.bean.UserProfile;
import kr.co.ahope.carwang.api.user.dao.UserAPIDAO;
import kr.co.ahope.common.web.client.HyundaiRestTemplate;
import kr.co.ahope.common.web.mvc.service.CommonAPIService;

@Service
public class UserAPIService extends CommonAPIService {

	@Autowired
	private HyundaiRestTemplate hyundaiRestTempalte;
	
	@Autowired
	private UserAPIDAO dao;
	
	@Value("${api.host}")
	private String API_HOST;
	
	@Value("${api.client.id}")
	private String API_CLIENT_ID;
	
	@Value("${api.client.secret}")
	private String API_CLIENT_SECRET;
	
	@Value("${redirect.uri.user}")
	private String REDIRECT_URI_USER;

	public User auth(String code) throws Exception {

		//token 발급
		String url = API_HOST + "/api/v1/user/oauth2/token";
		String paramStr = "grant_type=authorization_code" //발급
							+"&code="+code
							+"&redirect_uri="+REDIRECT_URI_USER;
		TokenResponse token = hyundaiRestTempalte.postHyundaiToken(url, API_CLIENT_ID, API_CLIENT_SECRET, paramStr, TokenResponse.class); 

		//token으로 사용자 정보 조회
		url = API_HOST + "/api/v1/user/profile";
		UserProfile userProfile = hyundaiRestTempalte.getHyundai(url, token.getAccessToken(), UserProfile.class);
		
		//api token 생성 
		String apiToken = UUID.randomUUID().toString();
		
		//사용자 정보 db 저장 및 갱신
		User user = new User();
		user.setApiToken(apiToken);
		user.setUserId(userProfile.getId());
		user.setAccessToken(token.getAccessToken());
		user.setRefreshToken(token.getRefreshToken());

		dao.upsertUser(user);
		
		user.setCarServiceAgreement(dao.checkCarServiceAgreement(user));
		return user;
	}
	
}
