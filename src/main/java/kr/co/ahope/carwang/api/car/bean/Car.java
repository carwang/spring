package kr.co.ahope.carwang.api.car.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class Car {
	
	@JsonInclude(Include.NON_NULL)
	private Integer no;
	private String carId;
	private String carType;
	private String carName;
	@JsonInclude(Include.NON_NULL)
	private String carNickname;
	
	@JsonIgnore
	private Long userSeq;
	@JsonIgnore
	private Long carSeq;
	@JsonInclude(Include.NON_NULL)
	private Double fuelEfficiency;
	@JsonInclude(Include.NON_NULL)
	private Integer fuelEfficiencyRank;
	@JsonInclude(Include.NON_NULL)
	private Double odometer;
	@JsonInclude(Include.NON_NULL)
	private Integer odometerRank;
	@JsonInclude(Include.NON_NULL)
	private Integer odometerUnit;
	
}
